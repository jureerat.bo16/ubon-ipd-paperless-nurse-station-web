import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NgZorroModule } from '../../ng-zorro.module';
import { CoreModule } from '../../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import {VariableShareService} from '../../core/services/variable-share.service';
import { BasicBarChartComponent } from './basic-bar-chart/basic-bar-chart.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { ColumnChartBasicComponent } from './column-chart-basic/column-chart-basic.component';
import { LineChartBasicComponent } from './line-chart-basic/line-chart-basic.component';
import { DonutChartBasicComponent } from './donut-chart-basic/donut-chart-basic.component';
import { SyncingChartComponent } from './syncing-chart/syncing-chart.component';


@NgModule({
  declarations: [
    DashboardComponent,BasicBarChartComponent, ColumnChartBasicComponent, LineChartBasicComponent, DonutChartBasicComponent, SyncingChartComponent
 
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroModule,
  
    CoreModule,
    SharedModule,
    DashboardRoutingModule,NgApexchartsModule
  ],
  providers: [VariableShareService]

})
export class DashboardModule { }
