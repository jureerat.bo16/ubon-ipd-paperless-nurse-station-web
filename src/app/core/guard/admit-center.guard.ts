import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';

export const admitCenterGuard: CanActivateFn = (route, state) => {
  const authService = inject(admitCenterGuard);
  return authService.isAllow();
};
